var gulp = require('gulp'); // Gulp
    yarn = require('gulp-yarn'); // Yarn плагин
    pug = require('gulp-pug'); // Pug плагин
    stylus = require('gulp-stylus'); // Stylus плагин
    myth = require('gulp-myth'); // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'); // Минификация CSS
    imagemin = require('gulp-imagemin'); // Минификация изображений
    uglify = require('gulp-uglify'); // Минификация JS
    concat = require('gulp-concat'); // Склейка файлов
    flatten = require('gulp-flatten'); // Изменение относительного пути для файлов
    browserSync = require('browser-sync').create(); // Виртуальный сервер
    reload = browserSync.reload; // Перезагрузка виртуального сервера при изменениях
    clean = require('gulp-clean'); // удаляет файлы и папки
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с PNG

// КОМАНДЫ
    start_server = 'server'; // запуск сбыстрой сборки на локальном сервере
    start_build = 'www'; // компиляция финального проекта

// ОСНОВНЫЕ ДИРЕКТОРИИ
    assets_dir = './assets/'; // разработка
    server_dir = './server/'; // готовый проект
    www_dir = './www/'; // сервер

// ПАПКИ ДЛЯ БЫСТРОЙ СБОРКИ
    css_server = './server/css/'; // CSS
    js_server = './server/js/'; // JS
    images_server = './server/images/'; // PNG, JPG и SVG
    fonts_server = './server/fonts/'; // шрифты
    pages_server = './server/pages/'; // страницы


// ПАПКИ ДЛЯ ГОТОВЫХ ФАЙЛОВ
    css_www = './www/css/'; // CSS
    js_www = './www/js/'; // JS
    images_www = './www/images/'; // PNG, JPG и SVG
    fonts_www = './www/fonts/'; // шрифты
    pages_www = './www/pages/'; // страницы

// РЕСУРСЫ
    main_pug_path = './assets/pages/index.pug'; // путь до основной страницы
    main_styl_path = './assets/basic/styles/index.styl'; // путь до основного файла со стилями
    pug_path = './assets/pages/**/*/'; // pug
    styl_path = './assets/**/*/*.styl'; // styles
    js_path = './assets/**/*/'; // JS
    images_path = './assets/**/*/*.+(png|jpg)'; // PNG и JPG
    svg_path = './assets/**/*/*.svg'; // SVG
    fonts_path = './assets/basic/fonts/**/*'; // шрифты

// Установка всех зависимостей проекта
gulp.task('yarn', function() {
    return gulp.src(['./package.json', './yarn.lock'])
        .pipe(gulp.dest('./dist'))
        .pipe(yarn({
            production: true
        }));
});

// С Е Р В Е Р
gulp.task(start_server, ['pug', 'stylus', 'js', 'imgfonts'], function() {
    browserSync.init({
        notify: false,
        server: server_dir
    });
    // Следим за изменениями в файлах
    gulp.watch('./assets/**/*', ['pug', 'stylus', 'js']);
});

// Собираем HTML из pug
gulp.task('pug', function() {
    gulp.src([main_pug_path])
        .pipe(pug({ pretty: true }))
        .on('error', console.log)
        .pipe(gulp.dest(server_dir))
        .pipe(reload({ stream: true }));
    gulp.src([pug_path + 'index.pug'])
        .pipe(pug({ pretty: true }))
        .on('error', console.log)
        .pipe(gulp.dest(server_dir ))
        .pipe(reload({ stream: true }));
});

// Собираем CSS из Stylus
gulp.task('stylus', function() {
    gulp.src(main_styl_path)
        .pipe(stylus({
            // use: ['nib']
            set: [{ 'linenos': true }]
        }))
        .on('error', console.log)
        .pipe(myth())
        .pipe(concat('index.css'))
        .pipe(clean({ force: true }))
        .pipe(gulp.dest(css_server))
        .pipe(reload({ stream: true }));
});

// Собираем JS в файл index.js
gulp.task('js', function() {
    gulp.src([js_path + '*.js', '!' + js_path + '_*.js'])
        .pipe(concat('index.js'))
        .pipe(clean({ force: true }))
        .pipe(gulp.dest(js_server))
        .pipe(reload({ stream: true }));
});
// Копируем изображения и шрифты
gulp.task('imgfonts', function() {

    // PNG и JPG
    gulp.src(images_path)
        .pipe(flatten({ includeParents: 0 }))
        .pipe(gulp.dest(images_server))

    // SVG
    gulp.src([svg_path, '!' + fonts_path])
        .pipe(flatten({ includeParents: 0 }))
        .pipe(gulp.dest(images_server))

    // Все шрифты
    gulp.src(fonts_path)
        .pipe(gulp.dest(fonts_server))
});

// Р Е Л И З
gulp.task(start_build, function() {
     // HTML
    gulp.src([main_pug_path])
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(www_dir))
    gulp.src([pug_path + 'index.pug'])
        .pipe(pug({ pretty: true }))
        .pipe(flatten({ includeParents: -1 }))
        .pipe(gulp.dest(www_dir))
    // CSS
    gulp.src(main_styl_path)
        .pipe(stylus({
            // use: ['nib']
            set: [{ 'linenos': true }]
        }))
        .pipe(myth())
        // .pipe(csso()) // минимизируем
        .pipe(gulp.dest(css_www))
    // JS
    gulp.src([js_path + '*.js', '!' + js_path + '_*.js'])
        .pipe(concat('index.js'))
        .pipe(uglify())
        .pipe(flatten({ includeParents: 0 }))
        .pipe(gulp.dest(js_www));

    // PNG и JPG
    gulp.src(images_path)
        .pipe(imagemin())
        .pipe(flatten({ includeParents: 0 }))
        .pipe(gulp.dest(images_www))

    // SVG
    gulp.src([svg_path, '!' + fonts_path])
        .pipe(flatten({ includeParents: 0 }))
        .pipe(gulp.dest(images_www))

    // fonts
    gulp.src(fonts_path)
        .pipe(gulp.dest(fonts_www))

});